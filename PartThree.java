//Polina Yankovich 1834306
import java.util.Scanner;
public class PartThree {
	public static void main (String[] args){
		Scanner reader= new Scanner(System.in);
//static method
		System.out.println("Enter a square side");
		double square= reader.nextDouble();
		System.out.println("Area of square is "+AreaComputations.areaSquare(square));
//instance method 

		System.out.println("Enter width of a Rectangle");
		double widthRect= reader.nextDouble();

		System.out.println("Enter length of a Rectangle");
		double lengthRect=reader.nextDouble();
		
AreaComputations rectangle= new AreaComputations();

		System.out.println("Area of Rectangle is "+rectangle.areaRectangle(widthRect,lengthRect));
}}
